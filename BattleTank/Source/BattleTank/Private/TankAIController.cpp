// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "Engine/World.h"
#include "Tank.h"


ATank* ATankAIController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}

ATank * ATankAIController::GetPlayerTank() const
{
	APlayerController* PlayerController0 = GetWorld()->GetFirstPlayerController();
	if (PlayerController0)
	{
		return Cast<ATank>(PlayerController0->GetPawn());
	}
	else
	{

		return nullptr;
	}
}

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
	//UE_LOG(LogTemp, Warning, TEXT("AIController Begin Play!"));

	ATank* ControlledTank = GetControlledTank();
	if (ControlledTank)
	{
		FString TankName = ControlledTank->GetName();
		UE_LOG(LogTemp, Warning, TEXT("AIController is controlling tank: %s"), *TankName);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AIController is not possesing a tank!"));
	}


	ATank* PlayerTank = GetPlayerTank();
	if (PlayerTank)
	{
		FString PlayerTankName = PlayerTank->GetName();
		UE_LOG(LogTemp, Warning, TEXT("AIController found player's tank: %s"), *PlayerTankName);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AIController could not find player's tank!"));
	}
	
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!GetPlayerTank()) return;

	// TODO Move towards the player.

	// Aim towards the player.
	GetControlledTank()->AimAt(GetPlayerTank()->GetActorLocation());

	// Fire if ready.
}



